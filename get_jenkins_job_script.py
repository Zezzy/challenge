#!/usr/bin/python

import sqlite3 as lite
import sys, os
from jenkinsapi.jenkins import Jenkins
from datetime import datetime

database = 'jenkins_jobs.db'

""" Get desired Jenkins server instance """
def get_jenkins_instance():
    jenkins_url = 'http://localhost:8080'
    jenkins_user = ''
    jenkins_pass = ''
    
    return Jenkins(jenkins_url, jenkins_user, jenkins_pass)

"""Get job details of each job that is running on the Jenkins instance"""
def get_job_details():
    jobs = []
    server = get_jenkins_instance()
    for j in server.get_jobs():
        job_instance = server.get_job(j[0])

        status = "%s, %s" %( 'enabled' if job_instance.is_enabled() else 'disabled', 'running' if job_instance.is_running() else 'not running')
        
        jobs.append((job_instance.name, job_instance.get_description(), status, datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    return jobs
        

con = None

try:
    if not os.path.exists('jenkins_jobs.db'):
        print "jenkins_jobs.db not found, creating database ...."
        con = lite.connect(database)
        print "creating TABLE, jobs ...."
        con.execute('CREATE TABLE jobs (name TEXT, description TEXT, status TEXT, date_time TEXT)')
    else:
        con = lite.connect(database)
    cur = con.cursor()
    jobs = get_job_details()

    print "inserting jobs status to db ...."
    cur.executemany('INSERT INTO jobs VALUES (?,?,?,?)', jobs)
    con.commit()
    
except lite.Error, e:
    
    print "Error %s:" % e.args[0]
    sys.exit(1)
    
finally:
    
    if con:
        print "closing connection"
        con.close()

from django.shortcuts import render
from django.shortcuts import redirect

from .forms import add_user_form
from .models import User

# Create your views here.
def home(request):
	template = 'base.html'
	return render(request,template)


def listUsers(request):
	context = {'users':User.objects.all()}
	template = 'list_users.html'
	return render(request, template, context)



def addUser(request):
	form = add_user_form(request.POST or None)
	if form.is_valid():
		new_user = form.save(commit=False)
		new_user.save()
		return redirect('/list')

	context = {"form":form}
	template = 'add_user.html'
	return render(request, template, context) 
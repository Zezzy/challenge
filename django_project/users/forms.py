
from django import forms
from .models import User

# class User_form(forms.Form):
# 	name = forms.CharField()
# 	email = forms.EmailField()


class add_user_form(forms.ModelForm):
	class Meta:
		model = User
		fields = '__all__'

	def clean_email(self):
		email = self.cleaned_data.get('email')
		name = self.cleaned_data.get('name')
		if email and User.objects.filter(email=email).count():
			raise forms.ValidationError(u'Email addresses already used')
		return email
from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.home, name='home'),
	url(r'^list/$', views.listUsers, name='list_users'),
	url(r'^add/$', views.addUser, name='add_user'),
]